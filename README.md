# Cumulus Linux to Arista vEOS EVPN Interop

Welcome to this network topology demonstrating EVPN and VxLAN interoperability between
switches running Cumulus Linux 3.7.11 and a single Arista vEOS switch running 4.23.1F.


## Prerequisites and Getting Started

 - [Vagrant](https://releases.hashicorp.com/vagrant/)
 - Approximately 10GB disk space
 - Approximately 20GB free RAM
 - Create an [Arista vEOS Vagrant Image](https://github.com/petercrocker/veos-lab-vagrant-libvirt) (with much thanks to [Marc Weisel](https://github.com/mweisel) for his work on converting vEOS to libvirt).

## vEOS Image Creation

First you need to download and convert an Arista vEOS image to Vagrant/Libvirt format.
https://github.com/petercrocker/veos-lab-vagrant-libvirt

Note this vEOS image has Eth1 set for DHCP, with a static MAC address of 44:38:39:00:00:56. This is to connect to the oob-mgmt-switch, and recieve a DHCP provided static IP address for out of band management access. This allows the config_restore.yml ansible playbook run from the oob-mgmt-server to push a full configuration after first boot. 

## Quick Start:

```
    git clone https://gitlab.com/petercrocker/cl-evpn-arista-interop.git
    cd cl-evpn-arista-interop
    vagrant up oob-mgmt-server oob-mgmt-switch
    vagrant up
    vagrant ssh oob-mgmt-server
    git clone https://gitlab.com/petercrocker/cl-evpn-arista-interop.git
    cd cl-evpn-arista-interop/automation/
    ansible-playbook setup.yml
    ansible-playbook config_restore.yml
```
